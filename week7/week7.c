#include<stdio.h>
#include<string.h>
#define String_Len 80

void Delete(char*);
void Insert(char*);
void Find(char*);
void Quit(char*);


void Delete(char *p1){
    char s[String_Len]; //istenilen kelime ya da kelimeler için kullanıcam
    printf("Please enter what do you want to delete :");
    gets(s); //belki birden fazla kelime silmek isterlerse diye gets kullanıyorum
    //p1=strtok(*p1, s);
    //strncpy(&p1[strlen(p1)], strtok(NULL, s));
    printf("%s\n", strtok(p1, s));
    printf("%s\n\n", strtok(NULL, s));
}

void Insert(char *p1){
    int position;
    char temp[String_Len];
    char s[String_Len]; //istenilen kelime ya da kelimeler için kullanıcam
    printf("Please enter what do you want to insert and which position :");
    scanf("%s %d",s,&position);
    
    strncpy(temp,p1,position);
    strcpy(p1+position,s);

}

void Find(char *p1){
    char item[80];
    printf("Please enter what do you want to find :");
    scanf("%s",item);
    int mainSize=strlen(p1);
    int itemSize=strlen(item);
    //printf("item = %s\np1=%s\nstrlen item=%d\n strlen p1=%d\n", item, p1, itemSize, mainSize);
    for(int i=0; i<mainSize; i++)
    if(!strncmp(p1+i, item, itemSize))
    {
        printf("'%s' found at %d", item, i);
        return;
    }
}
void Quit(char *p1){
    int i;
    printf("String after editing : ");
    for (i=0; p1[i]!='0'; i++){
        printf("%c",p1[i]);
    }
}


int main(){
    char line[String_Len],*p1;
    p1 = line;
    char harf;

    printf("Enter the source string :");
    gets(line);
    //printf(line); for control
    printf("Enter D(Delete),I(Insert),F(Find),Q(Ouit)");
    scanf("%c",&harf);
    switch (harf)
    { //bankalardaki gibi
    case 'D':
    case 'f':
        Delete(p1);
        break;
    case 'I':
    case 'ı':
        Insert(p1);
        break;
    case 'F':
    case 'f':
        Find(p1);
        break;
    case 'Q':
    case 'q':
        Quit(p1);
        break;
    
    default:
        printf("Please enter available function!");
        break;
    }

    return 0;

}