#include "CipherMethods.h"
#include <stdio.h>
#include <string.h>



char * encryptCaesar(const char * orjinal){
  int length = strlen(orjinal);
  char* n=malloc(length * sizeof(char));
   for (int i=0; i< length; i++){
      if(strchr(CIPHER_SPECIAL_CHARS, orjinal[i]) != NULL)
        n[i] = orjinal[i];
      else if (isupper(orjinal[i])){
        if(orjinal[i] + CAESAR_SHIFT >  'Z')
          n[i] = orjinal[i] + CAESAR_SHIFT - NUMBER_OF_LETTERS;
        else
          n[i]=orjinal[i] + CAESAR_SHIFT;
      }
      else if (islower(orjinal[i])){
        if(orjinal[i] + CAESAR_SHIFT > 'z')
          n[i] = orjinal[i] + CAESAR_SHIFT - NUMBER_OF_LETTERS;
        else
          n[i]=orjinal[i] + CAESAR_SHIFT;
      }
      else if(isdigit(orjinal[i])){
        if(orjinal[i] + CAESAR_SHIFT >  '9')
          n[i] = orjinal[i] + CAESAR_SHIFT - NUMBER_OF_DIGITS;
        else
          n[i]=orjinal[i] + CAESAR_SHIFT;        
      }
   }
   return n;
}
 
char * decryptCaesar(const char * orjinal){
  int length = strlen(orjinal);
  char* n=malloc(length * sizeof(char));
   for (int i=0; i < length; i++){
      if(strchr(CIPHER_SPECIAL_CHARS, orjinal[i]) != NULL)
        n[i] = orjinal[i];
      else if (isupper(orjinal[i])){
        if(orjinal[i] - CAESAR_SHIFT <  'A')
          n[i] = orjinal[i] - CAESAR_SHIFT + NUMBER_OF_LETTERS;
        else
          n[i]=orjinal[i] - CAESAR_SHIFT;
      }
      else if (islower(orjinal[i])){
        if(orjinal[i] - CAESAR_SHIFT <  'a')
          n[i] = orjinal[i] - CAESAR_SHIFT + NUMBER_OF_LETTERS;
        else
          n[i]=orjinal[i] - CAESAR_SHIFT;
      }
      else if(isdigit(orjinal[i])){
        if(orjinal[i] - CAESAR_SHIFT <  '0')
          n[i] = orjinal[i] - CAESAR_SHIFT + NUMBER_OF_DIGITS;
        else
          n[i]= orjinal[i] - CAESAR_SHIFT;        
      }
   }
   return n;
}