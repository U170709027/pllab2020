#define TRACE_ORG_DEC
#include "CipherMethods.h"
#include <stdio.h>
#include <string.h>

void clearScreen() {
#ifdef WINDOWS
  system("cls");
#else
  system ("clear");
#endif
}

int main(void){
  char text[500];
  char passcode[9] = "ceng2002";

  
  printf("** You are about to enter a very secret cryptography service called CENG 2002 C-Secret Coded System **\n");
 
  printf("Enter your text:\n");
  fgets(text, 500, stdin);
  clearScreen();
  printf("In order to see the encrypted message, enter your passcode:\n");
  char ps[9];
  scanf("\n%s",ps);
  int i=1;
  for(int k=0; k<sizeof(ps); k++){
    if(ps[k]!=passcode[k]){
      printf("Wrong passcode. Enter again:\n");
      scanf("\n%s",ps);
      i++;
      if(i==3){
        printf("\nNumber of allowed attempts has been reached without successful entry!\nYour IP has been blacklisted by us. Good luck!! At least until your dynamic IP changes by tomorrow :)\n");
        k=sizeof(ps);
      }
    }
  } 
  
  //printf("sultan");
  char * encrypted=encryptCaesar(text);
  printf("Cipher: %s\n\n",encrypted);
  printf("Would you like to convert the cipher to original text (Y/N)?");
  char answer;
  scanf("\n%c",&answer);
  char * decrypted=decryptCaesar(encrypted);
  if(answer=='y' || answer=='Y'){
    printf("Cipher: %s\n", decrypted);
  }
  
#ifdef TRACE_ORG_DEC
 if(strncmp(text, decrypted, strlen(text)-1)==0){
   printf("\nOriginal text and the decrypted text match.\n");
 }else{
   printf("There is something wrong. Original text and the decrypted text do not match.\n");
 }
#endif
  
  return 0;
  }